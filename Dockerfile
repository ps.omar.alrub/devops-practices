FROM openjdk:8-jdk-alpine


ENV SPRING_PROFILES_ACTIVE=default
ENV JAVA_OPTS=""

WORKDIR /app


COPY target/*.jar /app/




CMD ["sh", "-c", "java $JAVA_OPTS -jar -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE *.jar"]

