#!/bin/bash

export COMMIT_SHA="$(git show -s --date=format:'%y%m%d' --format='%cd')-$(git rev-parse --short=8 HEAD)"

sed -i "s|image: super23/my-java-app:.*|image: super23/my-java-app:${COMMIT_SHA}|" spring-boot.yaml

