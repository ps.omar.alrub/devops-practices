#!/bin/bash

# Get the last commit date
last_commit_date=$(git log -1 --date=format:%y%m%d --pretty=format:%ad)

# Get the first eight digits of the commit hash
commit_hash=$(git log -1 --pretty=format:%h)

# Calculate the new version
new_version="$last_commit_date-$commit_hash"

# Update the version in the pom.xml file
mvn versions:set -DnewVersion="$new_version"

# Commit the changes to the pom.xml
git add pom.xml
git commit -m "Update application version to $new_version"
